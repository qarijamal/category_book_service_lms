/*
SQLyog Ultimate v11.33 (64 bit)
MySQL - 5.6.25-log : Database - category_book_service_lms_db
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`category_book_service_lms_db` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `category_book_service_lms_db`;

/*Table structure for table `category_book` */

DROP TABLE IF EXISTS `category_book`;

CREATE TABLE `category_book` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

/*Data for the table `category_book` */

insert  into `category_book`(`id`,`name`) values (1,'Java 1549568837436'),(2,'C#'),(3,'Java updated'),(4,'C# 4'),(5,'C# 5'),(6,'C# 6'),(7,'PHP 6'),(8,'PHP 7'),(9,'Java 1549570060027'),(10,'PHP 7'),(11,'PHP 7'),(12,'PHP 7'),(13,'PHP 7'),(14,'PHP 7'),(15,'PHP 7'),(16,'PHP 7'),(17,'PHP 7'),(24,'Java 1549570686238'),(19,'PHP 7'),(20,'C updated'),(21,'C++ updated'),(22,'.net updated'),(23,'Java updated');

/*Table structure for table `hibernate_sequence` */

DROP TABLE IF EXISTS `hibernate_sequence`;

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `hibernate_sequence` */

insert  into `hibernate_sequence`(`next_val`) values (25);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
