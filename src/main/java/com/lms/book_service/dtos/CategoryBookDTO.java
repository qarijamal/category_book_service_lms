package com.lms.book_service.dtos;

public class CategoryBookDTO {
    private Long id;
    private String name;

    public CategoryBookDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
