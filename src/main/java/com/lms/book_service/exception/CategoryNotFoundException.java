package com.lms.book_service.exception;

public class CategoryNotFoundException extends CategoryBookException {
    public CategoryNotFoundException(int code, String message) {
        super(code, message);
    }
}
