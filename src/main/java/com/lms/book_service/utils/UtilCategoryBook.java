package com.lms.book_service.utils;

import com.fasterxml.jackson.databind.ObjectMapper;

public class UtilCategoryBook {
    public static String jsonString(final Object obj) {
        try {
            return new ObjectMapper().writer().withDefaultPrettyPrinter().writeValueAsString(obj);
        } catch (Exception exception) {
            throw new RuntimeException(exception);
        }
    }
}
