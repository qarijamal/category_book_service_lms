package com.lms.book_service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CategoryBookServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CategoryBookServiceApplication.class, args);
	}

}

