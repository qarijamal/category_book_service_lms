package com.lms.book_service.repos;

import com.lms.book_service.entities.CategoryBook;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryBookRepo extends JpaRepository<CategoryBook, Long> {
    CategoryBook getCategoryBookById(long id);
}
