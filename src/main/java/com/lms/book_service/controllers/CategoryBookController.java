package com.lms.book_service.controllers;

import com.lms.book_service.dtos.CategoryBookDTO;
import com.lms.book_service.exception.CategoryNotFoundException;
import com.lms.book_service.services.CategoryBookManager;
import com.lms.book_service.vos.CategoryBookVO;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/")
public class CategoryBookController {
    private static Logger logger = LoggerFactory.getLogger(CategoryBookController.class);
    @Autowired
    private CategoryBookManager categoryBookManager;

    @PostMapping("category")
    public ResponseEntity<?> saveCategory(@RequestBody CategoryBookVO categoryBookVO) throws CategoryNotFoundException {
        logger.info("saveCategory called.......");
        this.categoryBookManager.saveCategory(new ModelMapper().map(categoryBookVO, CategoryBookDTO.class));
        logger.info("saveCategory ended.......");
        return new ResponseEntity(HttpStatus.CREATED);
    }

    @GetMapping("category/{id}")
    public ResponseEntity<?> getCategory(@PathVariable("id") long id) throws CategoryNotFoundException {
        logger.info("getCategory called.......");
        CategoryBookDTO categoryBookDTO = this.categoryBookManager.getCategory(id);
        logger.info("getCategory ended.......");
        return new ResponseEntity(categoryBookDTO, null, HttpStatus.OK);
    }

    @PutMapping("category")
    public ResponseEntity<?> update(@RequestBody CategoryBookVO categoryBookVO) throws CategoryNotFoundException {
        logger.info("updateCategory called.......");
        if (categoryBookVO.getId() == null || categoryBookVO.getId() <= 0) {
            throw new CategoryNotFoundException(HttpStatus.NOT_FOUND.value(), "CategoryBook id required");
        }
        categoryException(categoryBookVO.getId());
        this.categoryBookManager.updateCategory(new ModelMapper().map(categoryBookVO, CategoryBookDTO.class));
        logger.info("updateCategory ended.......");
        return new ResponseEntity(HttpStatus.OK);
    }

    @DeleteMapping("category/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") long id) {
        logger.info("delete  called.......");
        this.categoryException(id);
        this.categoryBookManager.delete(id);
        logger.info("delete  ended.......");
        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping("category/all")
    public ResponseEntity<?> getCategories() {
        logger.info("getCategories called.......");
        return new ResponseEntity(this.categoryBookManager.getCategories(), null, HttpStatus.OK);
    }

    private void categoryException(long categoryId) throws CategoryNotFoundException {
        if (categoryId <= 0) {
            throw new CategoryNotFoundException(HttpStatus.NOT_FOUND.value(), "Required category id.");
        }
    }
}
