package com.lms.book_service.services;

import com.lms.book_service.dtos.CategoryBookDTO;
import com.lms.book_service.exception.CategoryNotFoundException;

import java.util.List;

public interface CategoryBookManager {
    String saveCategory(CategoryBookDTO categoryBookDTO) throws CategoryNotFoundException;

    String updateCategory(CategoryBookDTO categoryBookDTO) throws CategoryNotFoundException;

    String delete(long id) throws CategoryNotFoundException;

    CategoryBookDTO getCategory(long id) throws CategoryNotFoundException;

    List<CategoryBookDTO> getCategories() throws CategoryNotFoundException;
}
