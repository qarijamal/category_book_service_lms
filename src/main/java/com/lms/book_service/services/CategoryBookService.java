package com.lms.book_service.services;

import com.lms.book_service.dtos.CategoryBookDTO;
import com.lms.book_service.entities.CategoryBook;
import com.lms.book_service.exception.CategoryNotFoundException;
import com.lms.book_service.repos.CategoryBookRepo;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class CategoryBookService implements CategoryBookManager {

    @Autowired
    private CategoryBookRepo categoryBookRepo;

    @Override
    public String saveCategory(CategoryBookDTO categoryBookDTO) throws CategoryNotFoundException {
        this.categoryBookRepo.save(new ModelMapper().map(categoryBookDTO, CategoryBook.class));
        return "";
    }

    @Override
    public String updateCategory(CategoryBookDTO categoryBookDTO) throws CategoryNotFoundException {
        CategoryBook categoryBook = this.categoryBookRepo.getCategoryBookById(categoryBookDTO.getId());
        if (categoryBook == null) {
            throw new CategoryNotFoundException(HttpStatus.NOT_FOUND.value(), "Category Not Found");
        }
        this.categoryBookRepo.save(new ModelMapper().map(categoryBookDTO, CategoryBook.class));
        return "";
    }

    @Override
    public String delete(long id) throws CategoryNotFoundException {
        CategoryBook categoryBook = this.categoryBookRepo.getCategoryBookById(id);
        if (categoryBook == null) {
            throw new CategoryNotFoundException(HttpStatus.NOT_FOUND.value(), "Category not found");
        }
        this.categoryBookRepo.delete(categoryBook);
        return "";
    }

    @Override
    public CategoryBookDTO getCategory(long id) throws CategoryNotFoundException {
        CategoryBook categoryBook = this.categoryBookRepo.getCategoryBookById(id);
        if (categoryBook == null) {
            throw new CategoryNotFoundException(HttpStatus.NOT_FOUND.value(), "Category not found");
        }
        return new ModelMapper().map(categoryBook, CategoryBookDTO.class);
    }

    @Override
    public List<CategoryBookDTO> getCategories() throws CategoryNotFoundException {
        List<CategoryBook> categoryBooks = this.categoryBookRepo.findAll();
        if (categoryBooks == null) {
            throw new CategoryNotFoundException(HttpStatus.NOT_FOUND.value(), "Categories not available.");
        }
        List<CategoryBookDTO> categoryBookDTOS = new ArrayList<>();
        for (CategoryBook categoryBook : categoryBooks) {
            categoryBookDTOS.add(new ModelMapper().map(categoryBook, CategoryBookDTO.class));
        }
        return categoryBookDTOS;
    }
}
