package category_book_service;

import com.lms.book_service.CategoryBookServiceApplication;
import com.lms.book_service.dtos.CategoryBookDTO;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Date;

import static com.lms.book_service.utils.UtilCategoryBook.jsonString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(SpringRunner.class)
@SpringBootTest(classes = CategoryBookServiceApplication.class)
public class CategoryBookControllerTest {
    private static String BASE_URL = "/api/";
    private MockMvc mockMvc;
    @Autowired
    private WebApplicationContext webApplicationContext;


    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void saveTest() throws Exception {
        CategoryBookDTO categoryBookDTO = new CategoryBookDTO();
        categoryBookDTO.setName("Java " + new Date().getTime());

        mockMvc.perform(
                post(BASE_URL + "category")
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                        .content(jsonString(categoryBookDTO)))
                .andExpect(status().isCreated())
        ;
    }


}
